<?php
/**
 * ----------------------------------------------------------
 * date: 2019/8/16 16:26
 * ----------------------------------------------------------
 * author: Raoxiaoya
 * ----------------------------------------------------------
 * describe:
 * ----------------------------------------------------------
 */

namespace Core;


use Logic\Factory;
use Logic\Model\StatConfigModel;

abstract class BasicCronBk
{
    public $log;
    public $logTag;
    public $actionArr;
    public $cacheKey;
    public $limit = 10;
    public $sleep = 1;
    public $tableKey = 'id';

    public function __construct(){
        $this->logTag = $this->logTag ?? basename(get_class());
        if(!isset($this->cacheKey)){
            die('cacheKey 没有设置');
        }
        $this->log = Factory::getLogger();
    }

    abstract function getList(): array;
    abstract function dealChangeRecord(array $record): bool;

    function main()
    {
        $this->beforeMain();

        while(true){
            $start = time();

            $this->initCacheKey();
            $data = $this->getList();
            $flag = true;
            if (!empty($data)) {
                foreach ($data as $record) {
                    $result = $this->dealChangeRecord($record);
                    if (!$result) {
                        $this->addFailList($record[$this->tableKey]);
                        $flag = false;
                    } else {
                        $this->addSucess($record[$this->tableKey]);
                    }
                }

                if (!$flag) {
                    $this->log->error($this->logTag, '部分记录处理失败');
                }

                $end = time();
                $this->log->info($this->logTag, '耗时: ' . ($end - $start) . ' seconds');

                if(isset($this->sleep) && is_int($this->sleep) && $this->sleep > 0){
                    sleep((int)$this->sleep);
                }

            }else{
                break;
            }
        }

        $this->endMain();
    }

    function countMain(){
        $start = time();
        $record = ['date'=>date('Y-m-d H:i:s')];
        $result = $this->dealChangeRecord($record);
        if(!$result){
            $this->log->error($this->logTag, '统计处理失败');
        }

        $end = time();
        $this->log->info($this->logTag, '耗时: ' . ($end - $start) . ' seconds');

        if(isset($this->sleep) && is_int($this->sleep) && $this->sleep > 0){
            sleep((int)$this->sleep);
        }
    }

    function beforeMain(){

    }

    function endMain(){

    }

    function initCacheKey(){
        $cacheStart = StatConfigModel::getInstance()->where('name', $this->cacheKey)->getOne();
        if(empty($cacheStart)){
            StatConfigModel::getInstance()->insert([
                'name'=>$this->cacheKey,
                'value'=>0,
            ]);
        }
    }

    function addFailList($primaryKey){
        $failList = StatConfigModel::getInstance()->where('name', $this->cacheKey)->getOne();
        if($failList['fail']){
            $update = $failList['fail'].','.$primaryKey;
        }else{
            $update = $primaryKey;
        }

        return StatConfigModel::getInstance()->where('name', $this->cacheKey)->update(['fail'=>$update]);
    }

    function addSucess($primaryKey){
        return StatConfigModel::getInstance()->where('name', $this->cacheKey)->update(['value'=>$primaryKey]);
    }
}