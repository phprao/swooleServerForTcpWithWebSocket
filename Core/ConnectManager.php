<?php
/**
 * ----------------------------------------------------------
 * date: 2019/10/24 18:00
 * ----------------------------------------------------------
 * author: Raoxiaoya
 * ----------------------------------------------------------
 * describe:
 * ----------------------------------------------------------
 */

namespace Core;


class ConnectManager
{
    use Singleton;

    /**
     * 存放连接，key=>fd，后期可迁至redis
     * @var array
     */
    public $connects = [];
    public $connectTable;
    const SECRET = 'fr%67_y8g';

    private function __construct($connectTable = null)
    {
        if(!isset($this->connectTable)){
            $this->connectTable = $connectTable;
        }
    }

    /**
     * 记录连接
     * @param $fd
     * @param $type 1-tcp, 2-websocket
     * @return bool
     */
    public function addConnect($fd, $type){
        $key = $this->createKey($fd);
        $this->connectTable->set($key, ['key'=>$key, 'fdId'=>$fd, 'code'=>'', 'type'=>$type]);
        return $key;
    }

    public function updateConnect($fd, $code){
        $key = $this->getKeyByFd($fd);
        if($this->connectTable->exist($key)){
            if($this->connectTable->set($key, ['code'=>$code])){
                return true;
            }
        }

        return false;
    }

    /**
     * 移除连接
     * @param $fd
     * @return bool
     */
    public function removeConnect($fd){
        $key = $this->getKeyByFd($fd);
        if($this->connectTable->exist($key)){
            $this->connectTable->del($key);
        }
        return true;
    }

    /**
     * 生产唯一key
     * @param $fd
     * @param string $code 可理解为机器授权码，机器码
     * @return string
     */
    public function createKey($fd){
        $key = $this->getKeyByFd($fd);
        if($this->connectTable->exist($key)){
            return $this->createKey($fd);
        }else{
            return $key;
        }
    }

    public function getKeyByFd($fd){
        return md5($fd . self::SECRET);
    }

    public function dump(){
        $arr = [];
        foreach($this->connectTable as $k => $row)
        {
            $arr[$k] = $row;
        }

        print_r($arr);
    }

    public function findRoomMember($fd){
        $key = $this->getKeyByFd($fd);
        $info = [];
        $fd = null;
        foreach($this->connectTable as $k => $row){
            if($k == $key){
                $info = $row;
                break;
            }
        }

        foreach($this->connectTable as $k => $row){
            if($row['code'] == $info['code'] && $k != $key){
                $fd = $row['fdId'];
                break;
            }
        }

        return $fd;
    }
}