<?php
/**
 * ----------------------------------------------------------
 * date: 2019/10/25 10:30
 * ----------------------------------------------------------
 * author: Raoxiaoya
 * ----------------------------------------------------------
 * describe:
 * ----------------------------------------------------------
 */

namespace Core;


trait Response
{
    public function tcpResponseSuccess($server, $fd, $data = '', $status = 0, $msg = '')
    {
        $res = [
            'status' => $status,
            'msg'    => $msg,
            'data'   => $this->formatData($data),
        ];
        $server->send($fd, json_encode($res));
    }

    public function tcpResponseError($server, $fd, $status = 0, $msg = '', $data = [])
    {
        $res = [
            'status' => $status,
            'msg'    => $msg,
            'data'   => $this->formatData($data),
        ];
        $server->send($fd, json_encode($res));
    }

    public function webSocketResponseSuccess($server, $fd, $data = [], $status = 0, $msg = '')
    {
        $res = [
            'status' => $status,
            'msg'    => $msg,
            'data'   => $this->formatData($data),
        ];
        $server->push($fd, json_encode($res));
    }

    public function webSocketResponseError($server, $fd, $status = 0, $msg = '', $data = [])
    {
        $res = [
            'status' => $status,
            'msg'    => $msg,
            'data'   => $this->formatData($data),
        ];
        $server->push($fd, json_encode($res));
    }

    public function formatData($data){
        $ret = null;
        if(is_array($data)){
            if(empty($data)){
                $ret = (object)$data;
            }else{
                $ret = $data;
            }
        }else{
            if(empty($data)){
                $ret = (object)[];
            }else{
                $ret = ['notice'=>$data];
            }
        }
        return $ret;
    }
}