<?php
/**
 * +----------------------------------------------------------
 * date: 2019/3/20 11:02
 * +----------------------------------------------------------
 * author: Raoxiaoya
 * +----------------------------------------------------------
 * describe:
 * +----------------------------------------------------------
 */

namespace Core;


use Lib\Config;
use Logic\Factory;
use Logic\Model\ChangeMoneyInfoModel;
use Logic\Model\CommonModel;
use Logic\Model\PlayerModel;
use Logic\Model\ChangeMoneyInfoRecordModel;
use Logic\Model\GameInfoModel;
use Logic\Model\PromoteInfoModel;
use Logic\Model\PlayerInfoModel;
use Logic\EsGroup\EsOfChangeMoneyRecord;
use Logic\Model\AgentInfoModel;

abstract class BaseAction
{
    public $log;

    public function __construct(){
        $this->log = Factory::getLogger();
    }

    abstract function getList(): array;
    abstract function dealChangeRecord(array $record): bool;

    function main()
    {
        while(true){
            $start = time();

            $data = $this->getList();
            $flag = true;
            if (!empty($data)) {
                foreach ($data as $record) {
                    $result = $this->dealChangeRecord($record);
                    if (!$result) {
                        $this->updateLogTime($record['change_money_id']);
                        $flag = false;
                    } else {
                        $this->deleteLog($record['change_money_id']);
                        $this->addChangeMoneyInfoRecord($record);
                    }
                }

                if (!$flag) {
                    $this->log->error($this->logTag ?? 'BaseAction', '部分记录处理失败');
                }

                $end = time();
                $this->log->info($this->logTag ?? 'BaseAction', '耗时: ' . ($end - $start) . ' seconds');

            }

            if(isset($this->sleep) && is_int($this->sleep) && $this->sleep > 0){
                sleep((int)$this->sleep);
            }
        }
    }

    function updateLogTime($id){
        return ChangeMoneyInfoModel::getInstance()
            ->where('change_money_id', $id)
            ->update(['change_money_update_time'=>time()]);
    }

    function deleteLog($id){
        return ChangeMoneyInfoModel::getInstance()->where('change_money_id', $id)->delete();
    }

    //判断是否是机器人
    function isRobot($player_id){
        $player_info = PlayerModel::getInstance()->where('player_id', $player_id)->getOne();
        return $player_info['player_robot'];
    }

    function addChangeMoneyInfoRecord($record){
        if($record['change_money_game_id']){
            $game_name = GameInfoModel::getInstance()->getGameName($record['change_money_game_id']);
        }
        if($record['change_money_club_room_id']){
            $room_name = CommonModel::getInstance('club_room')->where('club_room_id', $record['change_money_club_room_id'])->getOne();
        }
        $promoteInfo = PromoteInfoModel::getInstance()->where('player_id', $record['change_money_player_id'])->getOne();
        $player_info = PlayerInfoModel::getInstance()->where('player_id', $record['change_money_player_id'])->getOne();

        $data = [
            'player_id'    => $record['change_money_player_id'],
            'agent_id'     => $promoteInfo['agent_id'],
            'partner_id'   => $promoteInfo['partner_id'],
            'parent_id'    => $promoteInfo['parent_id'],
            'club_room_id' => $record['change_money_club_room_id'],
            'room_name'    => isset($room_name) ? $room_name['club_room_name'] : '',
            'club_room_no' => $record['change_money_club_room_no'],
            'game_id'      => $record['change_money_game_id'],
            'game_name'    => $game_name ?? '',
            'room_id'      => $record['change_money_room_id'],
            'type'         => $record['change_money_type'],
            'tax'          => $record['change_money_tax'],
            'water'        => $record['change_money_water'],
            'money_type'   => $record['change_money_money_type'],
            'money_value'  => $record['change_money_money_value'],
            'begin_value'  => $record['change_money_begin_value'],
            'after_value'  => $record['change_money_after_value'],
            'safe_box'     => $player_info['player_safe_box'],
            'ext'          => $this->getTypeExt($record),
            'time'         => $record['change_money_time'],
            'param'        => is_array($record['change_money_param']) ? json_encode($record['change_money_param']) : $record['change_money_param']
        ];

        $data = array_merge($data, $this->getRelation($record, $promoteInfo));

        $r1 = ChangeMoneyInfoRecordModel::getInstance()->insert($data);
        $r2 = true;
        if(Config::$use_es){
            $r2 = $this->addElasticsearchRecord($data);
        }

        return $r1 && $r2;
    }

    protected function getRelation($record, $promoteInfo): array{
        $agent = AgentInfoModel::getInstance()->where('id', $promoteInfo['agent_id'])->getOne();
        $agents = [
            'share_rate'     => $agent['share_rate'],
            'agent_get'      => $agent['share_rate'] * $record['change_money_tax'],
            'p_agent'        => $agent['p_id'],
            'p_share_rate'   => 0,
            'p_agent_get'    => 0,
            'p_p_agent'      => $agent['p_p_id'],
            'p_p_share_rate' => 0,
            'p_p_agent_get'  => 0,
        ];

        if($agent['p_id'] > 0){
            $p_agent = AgentInfoModel::getInstance()->where('id', $agent['p_id'])->getOne();
            $agents['p_share_rate'] = $p_agent['share_rate'];
            $agents['p_agent_get'] = $p_agent['share_rate'] * $record['change_money_tax'];
        }

        if($agent['p_p_id'] > 0){
            $p_p_agent = AgentInfoModel::getInstance()->where('id', $agent['p_p_id'])->getOne();
            $agents['p_p_share_rate'] = $p_p_agent['share_rate'];
            $agents['p_p_agent_get'] = $p_p_agent['share_rate'] * $record['change_money_tax'];
        }

        return $agents;
    }

    function addElasticsearchRecord($data){
        if(!Config::$use_es){
            return true;
        }

        $re = EsOfChangeMoneyRecord::getInstance()->insertLog($data);

        return $re;
    }

    function getTypeExt($record){
        switch($record['change_money_type']){
            case 1:
                $param = json_decode($record['change_money_param'], true);
                if(isset($param['order_id']) && $param['order_id'] > 0){
                    // 线上充值
                    $ext = 2;
                }elseif(isset($param['agent_id']) && $param['agent_id'] > 0){
                    // 代理充值
                    $ext = 1;
                }else{
                    // 总后台充值
                    $ext = 3;
                }
                break;
            default:
                $ext = '';
        }

        return $ext;
    }

}