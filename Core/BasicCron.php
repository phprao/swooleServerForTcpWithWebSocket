<?php
/**
 * +----------------------------------------------------------
 * date: 2019/3/20 18:07
 * +----------------------------------------------------------
 * author: Raoxiaoya
 * +----------------------------------------------------------
 * describe:
 * +----------------------------------------------------------
 */

namespace Core;


use Logic\Factory;

abstract class BasicCron
{
    public $log;

    abstract function getListByPage($page): array;
    abstract function getCount(): int;
    abstract function dealRecord(array $record): bool;

    function __construct()
    {
        $this->log = Factory::getLogger();
    }

    function main()
    {
        $this->log = Factory::getLogger();
        $start = time();

        $count = $this->getCount();
        $page = ceil($count / ($this->size ?? 10));
        $flag = true;
        $temppage = 1;

        while($temppage <= $page){
            $data = $this->getListByPage($temppage);
            foreach ($data as $record) {
                $result = $this->dealRecord($record);
                if (!$result) {
                    $flag = false;
                }
            }

            $temppage++;
        }

        if (!$flag) {
            $this->log->error($this->logTag ?? 'BasicCron', '部分记录处理失败');
        }

        $end = time();
        $this->log->info($this->logTag ?? 'BasicCron', '耗时: ' . ($end - $start) . ' seconds');
    }
}