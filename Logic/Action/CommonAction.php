<?php
/**
 * ----------------------------------------------------------
 * date: 2019/10/25 11:07
 * ----------------------------------------------------------
 * author: Raoxiaoya
 * ----------------------------------------------------------
 * describe:
 * ----------------------------------------------------------
 */

namespace Logic\Action;


use Core\Response;
use Core\Singleton;
use Core\ConnectManager;
use Lib\ErrorCode;

class CommonAction
{
    use Singleton;
    use Response;

    private function __construct($params = [])
    {

    }

    public function registerMachineCode($server, $fd, $reactorId, $data){
        $ConnectManager = ConnectManager::getInstance($server->connectTable);
        $res = $ConnectManager->updateConnect($fd, $data['actiondata']['code']);

        $ConnectManager->dump();

        if($res){
            return '注册成功';
        }else{
            throw new \Exception(ErrorCode::$errorMsg[ErrorCode::SYS_ERROR_2], ErrorCode::SYS_ERROR_2);
        }
    }

    public function walkForward($server, $fd, $reactorId, $data){
        $otherFd = ConnectManager::getInstance($server->connectTable)->findRoomMember($fd);
        if(!$otherFd){
            throw new \Exception(ErrorCode::$errorMsg[ErrorCode::MEMBER_IS_OFF], ErrorCode::MEMBER_IS_OFF);
        }
        // 发指令
        $this->tcpResponseSuccess($server, $otherFd, '你收到指令-'.$data['actioncode']);

        return '指令已发出';
    }
}