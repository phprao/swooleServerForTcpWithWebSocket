<?php

namespace Logic;


use Core\Singleton;
use Lib\ErrorCode;

class Action
{
    use Singleton;

    public function doAction($server, $fd, $reactorId, $data)
    {
        $data = json_decode($data, true);
        if (isset(ActionRouter::$router[$data['actioncode']])) {

            $router = ActionRouter::$router[$data['actioncode']];

            if (is_string($router['uses']) && class_exists($router['uses'])) {

                // $reflection = new \ReflectionClass ($router['uses']);
                // $ins =  $reflection->newInstanceArgs ( $router['params'] );// 实例化，传入的是关联数组

                $class  = (string)$router['uses'];
                $method = (string)$router['method'];
                $ins    = $class::getInstance($router['params']);

                $ret = call_user_func([$ins, $method], $server, $fd, $reactorId, $data);
                return $ret;

            } else {

                throw new \Exception(ErrorCode::$errorMsg[ErrorCode::SYS_ERROR], ErrorCode::SYS_ERROR);

            }

        } else {

            throw new \Exception(ErrorCode::$errorMsg[ErrorCode::ACTION_NOT_FOUND], ErrorCode::ACTION_NOT_FOUND);

        }
    }
}