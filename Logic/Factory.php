<?php

namespace Logic;

use Lib\Logger;
use Lib\Config;
use Lib\RedisDriver;
use Lib\RedisClient;
use Elasticsearch\ClientBuilder;

class Factory
{
    protected static $db;
    protected static $log;
    protected static $es;

    static function getDb(){
        if(!isset(self::$db)){
            $conf = array_values(Config::$mysql_config);
            self::$db = new \MysqliDb(...$conf);
        }

        return self::$db;
    }

    static function getLogger(){
        if(!isset(self::$log)){
            self::$log = new Logger();
        }
        return self::$log;
    }

    static function getPlayerRedis($i){
        return new RedisDriver($i);
    }

    static function getRedisClient($i, $name){
        return new RedisClient($i, $name);
    }

    static function getEsClient(){
        if(!isset(self::$es)){
            $conf = Config::$es_config;
            self::$es = $client = ClientBuilder::create()->setHosts([$conf['ip'] . ':' . $conf['port']])->build();
        }

        return self::$es;
    }
}