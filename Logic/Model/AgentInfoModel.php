<?php

namespace Logic\Model;


use Core\Singleton;

class AgentInfoModel extends BaseModel
{
    use Singleton;

    function getOneLog($where)
    {
        $this->where('id', $where['id']);

        return $this->getOne();
    }
}