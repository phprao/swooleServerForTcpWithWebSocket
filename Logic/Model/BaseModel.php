<?php
/**
 * +----------------------------------------------------------
 * date: 2019/3/13 15:51
 * +----------------------------------------------------------
 * author: Raoxiaoya
 * +----------------------------------------------------------
 * describe:
 * +----------------------------------------------------------
 */

namespace Logic\Model;


use Lib\Config;
use Lib\Helper;
use Logic\Factory;

class BaseModel
{
    protected $pageLimit = 10;
    protected $table;
    protected $tag = 'sql';
    protected $filterMethod = [
        'insert', 'delete', 'update', 'get', 'getOne', 'getValue', 'insertMulti', 'replace', 'paginate'
    ];
    protected $queryMethod = [
        'rawQuery', 'rawQueryOne', 'rawQueryValue', 'inc', 'dec'
    ];
    protected $otherMethod = [
        'commit', 'rollback', 'startTransaction'
    ];

    protected function getTableName(){
        if(!isset($this->table)){
            $modelName = str_replace('Model', '', basename(str_replace('\\', '/', get_class($this))));
            $this->table = Config::$table_prefix . Helper::divideString($modelName);
        }
    }

    function setModel($modelName = ''){
        $this->table = Config::$table_prefix . $modelName;
    }

    function __call($name, $arguments)
    {
        $this->getTableName();

        $mysql = Factory::getDb();
        $log = Factory::getLogger();
        $mysql->pageLimit = $this->pageLimit;

        if(in_array($name, $this->filterMethod)){

            $result = $mysql->$name($this->table, ...$arguments);

            if(Config::$log_sql){
                $log->sql($this->tag, $mysql->getLastQuery());
            }

            if($mysql->getLastErrno()){
                $log->sql($this->tag, $mysql->getLastErrno() . ' - ' . $mysql->getLastError());
            }

            return $result;
        }elseif(in_array($name, $this->queryMethod)){
            $result = $mysql->$name(...$arguments);

            if(Config::$log_sql){
                $log->sql($this->tag, $mysql->getLastQuery());
            }

            if($mysql->getLastErrno()){
                $log->sql($this->tag, $mysql->getLastErrno() . ' - ' . $mysql->getLastError());
            }

            return $result;
        }elseif(in_array($name, $this->otherMethod)){
            $result = $mysql->$name();

            //if(Config::$log_sql){
            //    $log->info($this->tag, $mysql->getLastQuery());
            //}

            if($mysql->getLastErrno()){
                $log->sql($this->tag, $mysql->getLastErrno() . ' - ' . $mysql->getLastError());
            }

            return $result;
        }else{

            $mysql->$name(...$arguments);

            if($mysql->getLastErrno()){
                $log->sql($this->tag, $mysql->getLastErrno() . ' - ' . $mysql->getLastError());
            }

            return $this;
        }
    }

    /**
     * where()
     *
     * 多个字段
     * $db->where ('id', 1);
     * $db->where ('login', 'admin');
     *
     * 字段相等
     * $db->where ('lastLogin = createdAt');
     *
     * 比较符
     * $db->where ('id', 50, ">=");
     *
     * BETWEEN / NOT BETWEEN
     * $db->where('id', Array (4, 20), 'BETWEEN');
     *
     * IN / NOT IN
     * $db->where('id', Array(1, 5, 27, -1, 'd'), 'IN');
     *
     * LIKE
     * $db->where ("fullName", 'John%', 'like');
     *
     * NULL
     * $db->where ("lastName", NULL, 'IS NOT');
     *
     * raw where
     * $db->where ("id != companyId");
     * $db->where ("DATE(createdAt) = DATE(lastLogin)");
     * $db->where ("(id = ? or id = ?)", Array(6,2));
     * $db->where ("login","mike");
     */

    /**
     * $db->orderBy("id","asc");
     * $db->orderBy("login","Desc");
     *
     * $db->groupBy ("name");
     *
     * $db->join("users u", "p.tenantID=u.tenantID", "LEFT");
     * $db->where("u.id", 6);
     *
     *
     */

}