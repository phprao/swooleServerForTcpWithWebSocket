<?php

namespace Logic\Model;


class CommonModel extends BaseModel
{
    private static $instance = [];

    static function getInstance(...$args)
    {
        if(isset($args[0]) && $args[0] && is_string($args[0])){
            if(!isset(self::$instance[$args[0]])){
                self::$instance[$args[0]] = new static(...$args);
            }

            return self::$instance[$args[0]];
        }else{
            if(!isset(self::$instance[0])){
                self::$instance[0] = new static(...$args);
            }

            return self::$instance[0];
        }
    }

    function __construct(string $modelName = '')
    {
        if(is_string($modelName) && $modelName){
            parent::setModel($modelName);
        }
    }
}