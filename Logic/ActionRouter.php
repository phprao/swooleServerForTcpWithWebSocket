<?php
/**
 * ----------------------------------------------------------
 * date: 2019/10/25 10:58
 * ----------------------------------------------------------
 * author: Raoxiaoya
 * ----------------------------------------------------------
 * describe:
 * ----------------------------------------------------------
 */

namespace Logic;


use Logic\Action\CommonAction;

class ActionRouter
{
    public static $router = [
        10001 => ['remark' => '注册机器码', 'uses' => CommonAction::class, 'method' => 'registerMachineCode', 'params' => []],
        10002 => ['remark' => '前进指令', 'uses' => CommonAction::class, 'method' => 'walkForward', 'params' => []],
    ];
}