<?php
/**
 * ----------------------------------------------------------
 * date: 2019/8/29 9:17
 * ----------------------------------------------------------
 * author: Raoxiaoya
 * ----------------------------------------------------------
 * describe: TCP同步客户端
 * ----------------------------------------------------------
 */

$client = new Swoole\Client(SWOOLE_SOCK_TCP, SWOOLE_SOCK_SYNC);

$ret = $client->connect('192.168.2.200', 9503, 0.1, 0);

if(!$ret){
    die('connect failed'.PHP_EOL);
}

$client->send('I have connected'.PHP_EOL);

$response = $client->recv();

echo $response.PHP_EOL;

$client->close();