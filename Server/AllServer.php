<?php
/**
 * ----------------------------------------------------------
 * date: 2019/10/24 15:19
 * ----------------------------------------------------------
 * author: Raoxiaoya
 * ----------------------------------------------------------
 * describe:
 * ----------------------------------------------------------
 */

/**
 * https://wiki.swoole.com/wiki/page/855.html
 */

namespace Server;

use Services\WebSocketService;
use Services\TcpService;

class AllServer
{
    public $server;
    const CONN_LENGTH = 65536;// 最大连接数

    public function __construct()
    {
        $this->server = new \Swoole\WebSocket\Server("0.0.0.0", 9502);
        $this->createTable();
    }

    public function createTable(){
        // swoole table
        $this->server->connectTable = new \Swoole\Table(self::CONN_LENGTH);
        $this->server->connectTable->column('key', \Swoole\Table::TYPE_STRING, 60);
        $this->server->connectTable->column('code', \Swoole\Table::TYPE_STRING, 200);
        $this->server->connectTable->column('fdId', \Swoole\Table::TYPE_INT, 4);
        $this->server->connectTable->column('type', \Swoole\Table::TYPE_INT, 1);
        $this->server->connectTable->create();
    }

    public function start()
    {
        // websocket callback
        $WebSocketService = WebSocketService::getInstance($this->server);
        $this->server->on('open',    [$WebSocketService, 'open']);
        $this->server->on('message', [$WebSocketService, 'message']);
        $this->server->on('close',   [$WebSocketService, 'close']);
        // $this->server->on('request', [new WebSocketService(), 'request']);

        $this->server->set([
            'worker_num'    => 8,
            'daemonize'     => false,
            'max_request'   => 10000,
            'dispatch_mode' => 2,
            'debug_mode'    => 1
        ]);

        // tcp server
        $port = $this->server->addlistener("0.0.0.0", 9503, SWOOLE_SOCK_TCP);

        $port->set([
            'package_length_offset' => 0,
        ]);
        $TcpService = TcpService::getInstance($this->server);
        $port->on('connect', [$TcpService, 'connect']);
        $port->on('receive', [$TcpService, 'receive']);
        $port->on('close',   [$TcpService, 'close']);

        $this->server->start();
    }
}