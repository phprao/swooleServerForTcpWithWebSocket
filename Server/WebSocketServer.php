<?php
/**
 * ----------------------------------------------------------
 * date: 2019/10/24 11:02
 * ----------------------------------------------------------
 * author: Raoxiaoya
 * ----------------------------------------------------------
 * describe:
 * ----------------------------------------------------------
 */

namespace Server;

class WebSocketServer
{
    // websocket server
    public $server;
    public $connects = [];

    public function __construct()
    {
        $server = new Swoole\WebSocket\Server("0.0.0.0", 9502);
        $this->server = $server;
    }

    /**
     * @param \Swoole\WebSocket\Server $server
     * @param $request http请求对象
     */
    public function open(Swoole\WebSocket\Server $server, $request){
        echo "server: handshake success with fd{$request->fd}\n";
        $server->push($request->fd, "connect success");
    }

    /**
     * @param \Swoole\WebSocket\Server $server
     * @param $frame WebSocket\Frame对象
     */
    public function message(Swoole\WebSocket\Server $server, $frame){
        echo "receive from {$frame->fd}:{$frame->data},opcode:{$frame->opcode},fin:{$frame->finish}\n";
        $server->push($frame->fd, "this is server");
    }

    /**
     * @param \Swoole\WebSocket\Server $server
     * @param $fd
     */
    public function close(Swoole\WebSocket\Server $server, $fd){
        echo "client {$fd} closed\n";
    }

    public function request($request, $response){
        // 接收http请求从get获取message参数的值，给用户推送
        // $this->server->connections 遍历所有websocket连接用户的fd，给所有用户推送
        echo '收到http指令'.PHP_EOL;
        foreach ($this->server->connections as $fd) {
            // 需要先判断是否是正确的websocket连接，否则有可能会push失败
            if ($this->server->isEstablished($fd)) {
                $this->server->push($fd, $request->get['message']);
            }
        }
    }

    public function start(){
        // 客户端与服务器建立连接并完成握手后会回调此函数
        $this->server->on('open', [$this, 'open']);

        // 服务器收到来自客户端的数据帧时会回调此函数
        $this->server->on('message', [$this, 'message']);

        // 客户端断开连接
        $this->server->on('close', [$this, 'close']);

        // 接收http请求，如果未定义此回调，默认返回http 400错误
        $this->server->on('request', [$this, 'request']);

        $this->server->set([
            'worker_num' => 8,
            'daemonize' => false,
            'max_request' => 10000,
            'dispatch_mode' => 2,
            'debug_mode' => 1
        ]);

        $this->server->start();
    }
}

(new WebSocketServer())->start();