<?php
/**
 * ----------------------------------------------------------
 * date: 2019/10/24 11:02
 * ----------------------------------------------------------
 * author: Raoxiaoya
 * ----------------------------------------------------------
 * describe:
 * ----------------------------------------------------------
 */

namespace Server;

class TcpServer
{
    public $server;
    public $connects = [];

    public function __construct()
    {
        $server = new Swoole\Server('0.0.0.0', 9503, SWOOLE_PROCESS, SWOOLE_SOCK_TCP);
        $this->server = $server;
    }

    public function connect($server, $fd){
        echo 'connect successful ...'.PHP_EOL;
    }

    public function receive($server, $fd, $reactorId, $data){
        echo 'receive data ...'.PHP_EOL;
        echo $data;
        echo $server->manager_pid.PHP_EOL;
        echo $server->master_pid.PHP_EOL;
        echo $server->worker_pid.PHP_EOL;
        $server->send($fd, "I am server".PHP_EOL);
    }

    public function close($server, $fd){
        echo 'close ...'.PHP_EOL;
    }

    public function start(){
        // 客户端与服务器建立连接并完成握手后会回调此函数
        $this->server->on('connect', [$this, 'connect']);

        // 服务器收到来自客户端的数据帧时会回调此函数
        $this->server->on('receive', [$this, 'receive']);

        // 客户端断开连接
        $this->server->on('close', [$this, 'close']);

        // 接收http请求，如果未定义此回调，默认返回http 400错误
        // $this->server->on('request', [$this, 'request']);

        $this->server->set([
            'worker_num' => 8,
            'daemonize' => false,
            'max_request' => 10000,
            'dispatch_mode' => 2,
            'debug_mode' => 1
        ]);

        $this->server->start();
    }
}

(new TcpServer())->start();