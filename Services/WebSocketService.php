<?php
/**
 * ----------------------------------------------------------
 * date: 2019/10/24 16:20
 * ----------------------------------------------------------
 * author: Raoxiaoya
 * ----------------------------------------------------------
 * describe:
 * ----------------------------------------------------------
 */

namespace Services;


use Core\ConnectManager;
use Core\Singleton;

class WebSocketService extends BaseService
{
    use Singleton;

    private function __construct()
    {

    }

    /**
     * 客户端与服务器建立连接并完成握手后会回调此函数
     * @param Swoole\WebSocket\Server $server
     * @param $request
     */
    public function open(\Swoole\WebSocket\Server $server, $request)
    {
        $ConnectManager = ConnectManager::getInstance($server->connectTable);
        $ConnectManager->addConnect($request->fd, self::CONNECT_FROM_WEBSOCKET);
        echo "websocket server: handshake success with fd {$request->fd}" . PHP_EOL;
        echo '新的连接ip: ' . $server->connections[$request->fd]['remote_ip'].PHP_EOL;
        $server->push($request->fd, "connect success");
    }

    /**
     * 服务器收到来自客户端的数据帧时会回调此函数
     * @param Swoole\WebSocket\Server $server
     * @param $frame
     */
    public function message(\Swoole\WebSocket\Server $server, $frame)
    {
        echo "websocket receive from {$frame->fd}:{$frame->data},opcode:{$frame->opcode},fin:{$frame->finish}" . PHP_EOL;
        $server->push($frame->fd, "this is server");
    }

    /**
     * 客户端断开连接
     * @param Swoole\WebSocket\Server $server
     * @param $fd
     */
    public function close(\Swoole\WebSocket\Server $server, $fd)
    {
        $ConnectManager = ConnectManager::getInstance($server->connectTable);
        $ConnectManager->removeConnect($fd);
        echo "websocket client {$fd} closed" . PHP_EOL;
    }

    /**
     * 接收http请求，如果未定义此回调，默认返回http 400错误
     * @param $request
     * @param $response
     */
    public function request($request, $response)
    {
        // 接收http请求从get获取message参数的值，给用户推送
        // $this->server->connections 遍历所有websocket连接用户的fd，给所有用户推送
        echo '收到http指令' . PHP_EOL;
        foreach ($this->server->connections as $fd) {
            // 需要先判断是否是正确的websocket连接，否则有可能会push失败
            if ($this->server->isEstablished($fd)) {
                $this->server->push($fd, $request->get['message']);
            }
        }
    }

    public static function __callStatic($name, $arguments)
    {
        return (new static)->$name(...$arguments);
    }
}