<?php
/**
 * ----------------------------------------------------------
 * date: 2019/10/24 16:20
 * ----------------------------------------------------------
 * author: Raoxiaoya
 * ----------------------------------------------------------
 * describe:
 * ----------------------------------------------------------
 */

namespace Services;


use Core\Response;
use Core\Singleton;
use Core\ConnectManager;
use Logic\Action;

class TcpService extends BaseService
{
    use Singleton;
    use Response;

    private function __construct()
    {

    }

    /**
     * 客户端与服务器建立连接并完成握手后会回调此函数
     * @param $server
     * @param $fd
     */
    public function connect($server, $fd)
    {
        $ConnectManager = ConnectManager::getInstance($server->connectTable);
        $key            = $ConnectManager->addConnect($fd, self::CONNECT_FROM_TCP);

        $ConnectManager->dump();
        echo '新的连接ip: ' . $server->connections[$fd]['remote_ip'].PHP_EOL;

        $this->tcpResponseSuccess($server, $fd, ['key' => $key]);
    }

    /**
     * 服务器收到来自客户端的数据时会回调此函数
     * @param $server
     * @param $fd
     * @param $reactorId
     * @param $data
     */
    public function receive($server, $fd, $reactorId, $data)
    {
        // echo 'tcp receive data ...' . PHP_EOL;
        // echo $data;
        // $server->send($fd, "I am server" . PHP_EOL);
        // //仅遍历 9514 端口的连接
        // $websocket = $server->ports[0];
        // foreach ($websocket->connections as $_fd) {
        //     if ($server->exist($_fd)) {
        //         $server->push($_fd, '来自tcp的指令');
        //     }
        // }

        try {
            $data = Action::getInstance()->doAction($server, $fd, $reactorId, $data);
            $this->tcpResponseSuccess($server, $fd, $data);
        } catch (\Exception $e) {
            $this->tcpResponseError($server, $fd, $e->getCode(), $e->getMessage());
        }

    }

    /**
     * 客户端断开连接
     * @param $server
     * @param $fd
     */
    public function close($server, $fd)
    {
        $ConnectManager = ConnectManager::getInstance($server->connectTable);
        $ConnectManager->removeConnect($fd);

        $ConnectManager->dump();
    }
}