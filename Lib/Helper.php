<?php

namespace Lib;


use Logic\Factory;
use Logic\Model\PlayerInfoModel;
use Server\AllServer;

class Helper
{
    static function divideString($string = ''){
        return strtolower(preg_replace('/(?<=[a-z])([A-Z])/', '_$1', $string));
    }

    static function changePlayerCoin($player_id, $coins){
        if(!$player_id || !$coins){
            return false;
        }
        $redis = Factory::getPlayerRedis($player_id);
        $a = $player_id % 1000;
        $b = (int)($a / 10);
        $fullKey = 'user_info:' . $b . ':' . $player_id;

        $exist = $redis->handler->exists($fullKey);
        if($exist){
            $after = $redis->handler->hincrby($fullKey, 'player_coins', $coins);
            if($after === false){
                return false;
            }

            return $after;
        }else{
            $db = PlayerInfoModel::getInstance();
            $re = $db->where('player_id', $player_id)->update(['player_coins'=>$db->inc($coins)]);
            if($re === false){
                return false;
            }

            $info = $db->where('player_id', $player_id)->getOne();

            return $info['player_coins'];
        }
    }

    static function package($data = []){

    }

    static function unPackage($raw = ''){

    }

    static function run(){
        (new AllServer())->start();
    }
}