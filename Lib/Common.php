<?php

use \Logic\Factory;

define('ROOT', dirname(__DIR__));

set_exception_handler('output_exception');
set_error_handler('output_error');
register_shutdown_function('output_shutdown');
spl_autoload_register('my_autoload');

date_default_timezone_set('Asia/Shanghai');
ini_set('error_reporting', 'E_ALL');

function output_exception($e){
    $logs = Factory::getLogger();
    $msg = $e->getFile() . ' | lime ' . $e->getLine() . ' | ' . $e->getMessage();
    $logs->error('sys_exception', $msg);
}
function output_error($errno, $errstr, $errfile, $errline){
    $logs = Factory::getLogger();
    $msg = $errfile . ' | lime ' . $errline . ' | ' . $errstr . ' | errno ' . $errno;
    $logs->error('sys_error', $msg);
}
function output_shutdown(){
    $logs = Factory::getLogger();
    $data = error_get_last();
    if(isset($data['message']) && !is_null($data['message'])) {
        $msg = $data['file'] . ' | line ' . $data['line'] . ' | ' . $data['message'];
        $logs->fatal('sys_shutdown', $msg);
    }
}

function my_autoload($class_name){
    $full = str_replace('\\', '/', $class_name);
    $file = ROOT . '/' . $full . '.php';
    if(file_exists($file)){
        require_once $file;
    }else{
        $ext = $full . '.php';
        require_once $ext;
    }

}

