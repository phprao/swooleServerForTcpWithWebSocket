<?php
/**
 * +----------------------------------------------------------
 * date: 2019/5/7 11:44
 * +----------------------------------------------------------
 * author: Raoxiaoya
 * +----------------------------------------------------------
 * describe:
 * +----------------------------------------------------------
 */

namespace Lib;


class RedisClient
{
    public $handler;

    function __construct($index = 0, $server = 'user_info') {
        $this->initDataRedis($index, $server);
    }

    public function initDataRedis($uid, $server) {
        $db = $uid % Config::$mod;
        if($server == 'user_info'){
            $redis_config = Config::$redis_user_info;
        }elseif($server == 'game_info'){
            $redis_config = Config::$redis_game;
        }else{
            $redis_config = Config::$redis_user_info;
        }

        $this->handler = new \redis();
        $ret = $this->handler->connect($redis_config['host'], $redis_config['port']);
        if (!$ret) {
            return false;
        }
        $ret = $this->handler->auth($redis_config['pass']);
        if (!$ret) {
            return false;
        }
        $ret = $this->handler->select($db);
        return true;
    }

    public function deinitDataRedis() {
        $this->handler->close();
    }
}