<?php

namespace Lib;

class Config {
	/* 数据库连接参数 */
	public static $mysql_config = array(
		'host'     => '192.168.0.21',
		'username' => 'root',
		'password' => 'magook123',
		'dbname'   => 'crmdata',
		'port'     => 3306,
		'charset'  => 'utf8',
	);
	/* Data redis 连接参数 */
	public static $mod = 10;
	public static $redis_crmdata = array(
		'host' => '192.168.0.21',
		'port' => 6379,
		'pass' => '',
		'db'   => 0
    );

	public static $use_es = false;
	public static $es_config = [
		'ip'=>'192.168.1.8',
		'port'=>9200,
	];

	public static $table_prefix = '';
	public static $log_sql = false;
	public static $max_log_num = 5;
	public static $clear_log = true;

	public static $stat_start_date = '2019-8-1';// 开始时间
}
