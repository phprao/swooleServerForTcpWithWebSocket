<?php
/**
 * ----------------------------------------------------------
 * date: 2019/10/25 11:18
 * ----------------------------------------------------------
 * author: Raoxiaoya
 * ----------------------------------------------------------
 * describe:
 * ----------------------------------------------------------
 */

namespace Lib;


class ErrorCode
{
    const ACTION_NOT_FOUND = 9404;
    const SYS_ERROR = 9503;
    const SYS_ERROR_2 = 9500;
    const MEMBER_IS_OFF = 9405;

    public static $errorMsg = [
        self::ACTION_NOT_FOUND => '指令不存在',
        self::SYS_ERROR        => 'Action实现类不存在',
        self::SYS_ERROR_2      => '系统出现异常',
        self::MEMBER_IS_OFF    => '对方不在线',
    ];
}